<?php

function printArr($arr){
	echo "arr{<br>";
	foreach ($arr as $key => $value){
		echo $key . ":" . $value . "<br>";
	}
	echo "}<br>";
}

function getWeightFromDate($dateString){

	$format = 'Y-m-d';
	$now = DateTime::createFromFormat($format, date($format));

	$birthday = DateTime::createFromFormat($format, $dateString);
	$age = $now->diff($birthday);
	//$weight = round($age->days/365.25, 2);
	$weight = (int)($age->days/365.25);

	return $weight;
}

function getDayFromDate($dateString){

	$format = 'Y-m-d';
	$date0 = DateTime::createFromFormat($format, '2014-01-01');
	
	$birthday = DateTime::createFromFormat($format, $dateString);
	$date = DateTime::createFromFormat($format, "2014-" . $birthday->format('m-d'));
	$days = $date0->diff($date);
	
	$day = $days->format('%a');
	
	return $day;
}

function getDateFromResult($result){

	$dayOfYear = (int)$result;

	$temp =  ($result - $dayOfYear) * 24;
	$hours = (int)$temp;

	$temp = ($temp - $hours) * 60;
	$minutes = (int)$temp;
	
	$temp = ($temp - $minutes) * 60;
	$seconds = (int)$temp;
	
	$arr = array();
	$arr["dayOfYear"] = $dayOfYear;
	$arr["hours"] = $hours;
	$arr["minutes"] = $minutes;
	$arr["seconds"] = $seconds;
	//printArr($arr);
	
	$currentYear = getdate()[year];

	$date = new DateTime($currentYear . '-01-01');
	$date->add(new DateInterval('P' . $dayOfYear . 'D'));
	$date->add(new DateInterval('PT' . $hours . 'H'));
	$date->add(new DateInterval('PT' . $minutes . 'M'));
	$date->add(new DateInterval('PT' . $seconds . 'S'));
	
	return $date;
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	$input=$_POST; 

	$name = $input['name1'];//"Roman";
	$name2 = $input['name2'];//"Jan";
	
	//TODO check values, filter html, etc
	
	//TODO for n:
	$dates = array($input['date1'], $input['date2']);

	$dataInputArray = array();
	$dataInputArray[$name] = $dates[0];
	$dataInputArray[$name2] = $dates[1];
	error_log(print_r($dataInputArray, TRUE));
	
	$sumOfWeights = 0;
	$sumOfWeightsTimesDays = 0;
	foreach ($dates as $date){
		$wi = getWeightFromDate($date);
		$xi = getDayFromDate($date);
		//error_log(print_r($xi, TRUE)); 
		$sumOfWeightsTimesDays += $wi * $xi;
		$sumOfWeights += $wi;
	}
	
	$result = $sumOfWeightsTimesDays/$sumOfWeights;
	//echo "result = " . $result . "<br>";

	$date = getDateFromResult($result);
	
	//error_log(print_r($sumOfWeights, TRUE)); 
	
	echo $date->format('Y-m-d H:i:s');
}	
?>